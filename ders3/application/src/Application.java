public class Application {

    private void cariHesapYaz() {
        tr.com.icbc.cari.Hesap mahmut = new tr.com.icbc.cari.Hesap();
        System.out.println(mahmut.toString());
    }

    private void krediHesapYaz() {
        tr.com.icbc.kredi.Hesap ahmet = new tr.com.icbc.kredi.Hesap();
        System.out.println(ahmet.toString());        
    }
    
    public void process() {
        cariHesapYaz();
        krediHesapYaz();
    }    

    public static void main(String[] args) {
        System.out.println("Application Started");

        Application application = new Application();
        application.process();
        
        System.out.println("Application Stopped");
    }

}
