
public class Kisi implements Musteri {

    private String adi;
    private String soyadi;

    public Kisi() {
        
    }
    
    public Kisi(String adi, String soyadi) {
        setAdi(adi);
        setSoyadi(soyadi);
    }
    
    public String getAdi() {
        return adi;
    }

    public void setAdi(String vAdi) {
        adi = vAdi;
    }

    public String getSoyadi() {
        return soyadi;
    }

    public void setSoyadi(String soyadi) {
        this.soyadi = soyadi;
    }
    
    public String getAdiSoyadi() {
        return adi + " " + this.soyadi;
    }
    
    public String getHitap(String unvan, String hitap) {
        return unvan + " " + hitap + " " + getAdiSoyadi();
    }

    @Override
    public double getBakiye() {
        return 10;
    }

}
