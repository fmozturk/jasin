
public class Yonetici extends Personel {

    private long yetkiSeviyesi;

    public long getYetkiSeviyesi() {
        return yetkiSeviyesi;
    }

    public void setYetkiSeviyesi(long yetkiSeviyesi) {
        this.yetkiSeviyesi = yetkiSeviyesi;
    }
    
}
