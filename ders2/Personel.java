
public class Personel extends Kisi implements Musteri {
    
    private int sicilNo;

    public int getSicilNo() {
        return sicilNo;
    }

    public void setSicilNo(int sicilNo) {
        this.sicilNo = sicilNo;
    }

    public String getHitap(String unvan, String hitap) {
        return Integer.toString(getSicilNo()) + " " + super.getHitap(unvan, hitap);
    }
    
    public double getBakiye() {
        return 20;
    }

}
