import java.math.BigDecimal;

public class MerhabaDunya {

    public void printBakiye(Musteri musteri) {
        System.out.println(musteri.getBakiye());
    }
    
    public void process() {
        Kisi kisi = new Kisi();
        printBakiye(kisi);
        
        Personel personel = new Personel();
        printBakiye(personel);
    }
    
    public static void main(String[] args) {
        MerhabaDunya service = new MerhabaDunya();
        service.process();
        
        double i = 0.3, j = 0.3, z = 0.3;
        System.out.println(i + j + z);
        
        BigDecimal deger = new BigDecimal(0.3);
        
        /*
        Kisi kisi = new Kisi("Emel", "Korkmaz");
        
        System.out.println(kisi.getAdi() + " " + kisi.getSoyadi());
        System.out.println(kisi.getAdiSoyadi());
        System.out.println(kisi.getHitap("Sayın", "Hanımefendi"));
        
        Personel personel = new Personel();
        personel.setAdi("Onur");
        personel.setSoyadi("Avcı");
        personel.setSicilNo(6662);
        System.out.println(personel.getHitap("Sayın", "Beyefendi"));
        */
    }
    
}
